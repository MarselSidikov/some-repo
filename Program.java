import java.util.Scanner;
import java.time.LocalDate;

class Program {
	public static void main(String[] args) {
		String prefix = "Hello! - ";
		Scanner scanner = new Scanner(System.in);
		String message = scanner.nextLine();
		System.out.println(prefix + message + " " + LocalDate.now());
	}
}
